package com.devcamp.task56_10.demo.controller;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.task56_10.demo.model.CDrink;
import com.fasterxml.jackson.annotation.JsonTypeInfo.Id;

@RestController
public class CDrinkController {
    @CrossOrigin
    @GetMapping("/ninh")
    public ArrayList<CDrink> getDrink(){
        //khai báo danh sách
        ArrayList<CDrink> listDrink = new ArrayList<CDrink>();
        LocalDate today = LocalDate.now(ZoneId.systemDefault());
        //khởi tạo đối tượng
        CDrink tratac = new CDrink(1,"TRATAC","Trà Tắc",10000, today,today);
        CDrink coca = new CDrink(1,"COCA","Cocacola",15000, today,today);
        CDrink pepsi = new CDrink(1,"PEPSI","Pepsi",20000, today,today);
        //thêm vào danh sách đối tượng
        listDrink.add(tratac);
        listDrink.add(coca);
        listDrink.add(pepsi);
        return listDrink;
    }

    
}
