package com.devcamp.task56_10.demo;

import java.time.LocalDate;

public class CDrink {
    int id;
    String maNuocUong;
    String tenNuocUong;
    long price;
    LocalDate ngayTao;
    LocalDate ngayCapNhat;
    public CDrink(){
        super();
    }
    //constructor
    public CDrink(int id, String maNuocUong, String tenNuocUong,long price, LocalDate ngayTao, LocalDate ngayCapNhat){
        this.id = id;
        this.maNuocUong = maNuocUong ;
        this.tenNuocUong = tenNuocUong;
        this.price = price;
        this.ngayTao = ngayTao;
        this.ngayCapNhat = ngayCapNhat;
    }
    // // get id
    // public int getId(){
    //     return id;
    // }
    // // set id
    // public void setId(int id){
    //     this.id
    // }
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getMaNuocUong() {
        return maNuocUong;
    }
    public void setMaNuocUong(String maNuocUong) {
        this.maNuocUong = maNuocUong;
    }
    public String getTenNuocUong() {
        return tenNuocUong;
    }
    public void setTenNuocUong(String tenNuocUong) {
        this.tenNuocUong = tenNuocUong;
    }
    public long getPrice() {
        return price;
    }
    public void setPrice(long price) {
        this.price = price;
    }
    public LocalDate getNgayTao() {
        return ngayTao;
    }
    public void setNgayTao(LocalDate ngayTao) {
        this.ngayTao = ngayTao;
    }
    public LocalDate getNgayCapNhat() {
        return ngayCapNhat;
    }
    public void setNgayCapNhat(LocalDate ngayCapNhat) {
        this.ngayCapNhat = ngayCapNhat;
    }
    
}
